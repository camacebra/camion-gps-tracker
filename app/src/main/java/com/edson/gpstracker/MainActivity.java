package com.edson.gpstracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST = 100;
    public EditText CamNo, Placa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button1 = (Button) findViewById(R.id.Send);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTrackerService();
            }
        });
        CamNo = (EditText) findViewById(R.id.editText);
        Placa = (EditText) findViewById(R.id.editText2);
        //Comprobar localizacion activada
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            finish();
        }
        //Comprobar permisos para acceder a la localizacion
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
        } else {
            //Si no pedir permiso//
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST);

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {

        //Permisos aceptados//
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        //Iniciar servicio de localizacion//
           // startTrackerService();
        } else {

        //si el usuario no acepta el permiso//

            Toast.makeText(this, "Acepta los permiso para acceder a la localizacion", Toast.LENGTH_SHORT).show();
        }
    }
    //Iniciar el servicio de localizacion
    private void startTrackerService() {
        Intent TrackInt = new Intent(MainActivity.this,TrackingService.class);
        String DatPlaca = Placa.getText().toString();
        String DatNoCam = CamNo.getText().toString();
        Bundle TrackBun = new Bundle();
        TrackBun.putString("Placa",DatPlaca);
        TrackBun.putString("CamionNo",DatNoCam);
        TrackInt.putExtras(TrackBun);
        startService(TrackInt);
    //Notificar al usuario que se activo lo localizacion//
        Toast.makeText(this, "GPS localizacion activada", Toast.LENGTH_SHORT).show();
    //Cerrar esta actividad//
        finish();
    }

}
