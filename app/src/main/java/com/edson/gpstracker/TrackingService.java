package com.edson.gpstracker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.IBinder;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.Manifest;
import android.location.Location;
import android.app.Notification;
import android.content.pm.PackageManager;
import android.app.PendingIntent;
import android.app.Service;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

public class TrackingService extends Service {
    public TrackingService() {
    }
    String CamionNo;
    String Placas;
    String localizacion;
    private static final String TAG = TrackingService.class.getSimpleName();
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Placas = intent.getStringExtra("Placa");
        CamionNo = intent.getStringExtra("CamionNo");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {

    return null;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {

        super.onCreate();
        buildNotification();
        loginToFirebase();
    }
    //Crear una notificacion siempre activa
    private void buildNotification() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            String NOTIFICATION_CHANNEL_ID="com.edson.gpstracker";
            String channelName = "GPS Tracking";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID,channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLACK);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert  manager != null;
            manager.createNotificationChannel(chan);
            String stop = "stop";
            registerReceiver(stopReceiver, new IntentFilter(stop));
            PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                    this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
            // Crear notificacion//
            Notification.Builder builder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.tracking_enabled_notif))//
                    .setOngoing(true)
                    .setContentIntent(broadcastIntent)
                    .setSmallIcon(R.drawable.ic_stat_name);
            startForeground(1, builder.build());
        }
        else {
            String stop = "stop";
            registerReceiver(stopReceiver, new IntentFilter(stop));
            PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                    this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
            // Crear notificacion permanente//
            Notification.Builder builder = new Notification.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.tracking_enabled_notif))
                    .setOngoing(true)
                    .setContentIntent(broadcastIntent)
                    .setSmallIcon(R.drawable.ic_stat_name);
            startForeground(1, builder.build());
        }

    }

    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(stopReceiver);
            stopSelf();
        }
    };
    private void loginToFirebase() {
        //Autentificacion con firebase (Correo y contraseña)//
        String email = getString(R.string.test_email);
        String password = getString(R.string.test_password);
        //llamar OnCompleteListener si el usuario y contraseña es correcto
        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(Task<AuthResult> task) {
                //Si el usuario se indentifico//
                if (task.isSuccessful()) {
                    //...llamar actluzacion de ubicacion//
                    requestLocationUpdates();
                } else {
                    //Si no es corrrecto entonces enviar mensaje de error//
                    Log.d(TAG, "Error de identificacion");
                }
            }
        });
    }
    private void requestLocationUpdates() {
        LocationRequest request = new LocationRequest();
        //Intervalo de actualizacion de 10 segundos//
        request.setInterval(10000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        final String path = CamionNo;
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        //Realizar solo si se tiene persmisos//
        if (permission == PackageManager.PERMISSION_GRANTED) {
            client.requestLocationUpdates(request, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
        //Obtener una referencia de la base de datos para escribir//
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference(path);
                    Location location = locationResult.getLastLocation();
                    localizacion = (String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()));
                    if (location != null) {
        //Guardar ubicacion en la base de datos//
                        ref.child("Ubicacion").setValue(localizacion);
                        ref.child("Placas").setValue(Placas);
                    }
                }
            }, null);
        }
    }
}
